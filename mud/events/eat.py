from .event import Event2


class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        if self.object.has_prop("eatable"):
	        self.object.move_to(None)
        	self.inform("eat")


