[33mcommit 5f755fcdfc7278be1b7f583c355382949fca255c[m
Author: Wallee <luciealphonsine@gmail.com>
Date:   Tue Mar 17 16:55:06 2015 +0100

    premier ajout

[33mcommit 978e553f699031cb466d34d98d46ed2f4ced6b49[m
Author: Wallee <luciealphonsine@gmail.com>
Date:   Tue Mar 17 16:54:22 2015 +0100

    ajout de salle-11-000

[33mcommit d6df1820f66c0b03925b71f887dd2242393e620c[m
Author: Wallee <luciealphonsine@gmail.com>
Date:   Tue Mar 17 16:39:18 2015 +0100

    ajout de la salle 10

[33mcommit 26d4109ffa815c628471791a1bae5500fd18f3d9[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Mar 10 15:18:56 2015 +0100

    allow composite words such as lampe-torche

[33mcommit d98946b0530246836eb15e2c6343b65962674997[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Thu Mar 27 19:17:11 2014 +0100

    find/resolve_for_operate should also look at parts

[33mcommit 9cb6b69e4bd0d420f231eda96d8c95ef89a146f7[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Mar 24 17:18:33 2014 +0100

    cant inspect inside a closed container

[33mcommit 930a5f0172033e2028d36d4239684b81eb315d81[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Mar 24 16:51:42 2014 +0100

    careful with contextual location

[33mcommit 407fc92459120882bcf637cf799f22df81c7e88b[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Mar 24 15:16:20 2014 +0100

    add location to the event context

[33mcommit a7a8da0b597fa51828a5f43b9ec3818124442ae8[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Fri Mar 21 16:39:06 2014 +0100

    raise exception on id collision

[33mcommit 1f243df547d4acd1f39d0eec980c18522a682172[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Thu Mar 20 23:29:01 2014 +0100

    support pour les actions: entrer et sortir

[33mcommit d26fa79d6b02cfa43b30ea1c8c19494c7c1b26e9[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Thu Mar 20 10:33:42 2014 +0100

    be more careful with cookies referring to non-existing users

[33mcommit b92264961ef324a5a9f9b655be013d077d63cb2e[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 19 17:13:19 2014 +0100

    typo

[33mcommit a18b9aa0bb06432f3a5f9f32dc1575e6f09ddc95[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 19 16:59:39 2014 +0100

    allow context extension for get_effects

[33mcommit eac7c28dc088c81fdfc4196d7e83a377a8748b11[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 19 14:00:04 2014 +0100

    LeavePortalEffect, MoveEffect, MoveEvent

[33mcommit a0f6f39e9a8568973dc381730c0ac71f2f6e06ee[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 19 13:58:37 2014 +0100

    make it possible to declaratively specify traversals

[33mcommit 45256d16449c372798df94ca5eddd8caa9655fa8[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 19 13:40:55 2014 +0100

    add 'the' to the context to deref ids.  allow context extension for get_datum

[33mcommit 633ff3d2421ba77680a7083d9cbc3ec47b3c0f4f[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Mar 18 17:09:38 2014 +0100

    added noun_of_the() and noun_of_a()

[33mcommit 1760e5da10e152639488c2397889055f35b44152[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Mar 17 18:21:35 2014 +0100

    don't execute effects of failed events

[33mcommit 0716b5cd3a0a5559f6e8e21b9feaab184121cf72[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Mar 17 12:33:31 2014 +0100

    typo

[33mcommit 978d9851f87af20c4381d80fe9a8f97338956b8c[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Thu Mar 13 10:59:10 2014 +0100

    added NarrativeEffect (for observers of remote effects)

[33mcommit 72fbb3239698765897a0418f56a30a18e704da8c[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 12 20:08:15 2014 +0100

    don't break if initial.yml contains an empty document

[33mcommit 1d634820d6db0ce87c933bb8a3bb73f1c1c28154[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 12 19:56:18 2014 +0100

    forgot to close an <i>

[33mcommit addbeaa077d9ac4727fee0dceb465b95edccb0c4[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Mar 10 18:14:43 2014 +0100

    use /usr/bin/env, /bin/env isn't available on ubuntu

[33mcommit cce0a7a73fa605e5687068e5cca48e1ffc1c6283[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sun Mar 9 20:14:39 2014 +0100

    replace initial.yml and static.yml by globs *initial*.yml and *static*.yml

[33mcommit b7db3e0fded9f48c6dc9b677856de0624aec9605[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sun Mar 9 19:50:25 2014 +0100

    move rules into game-specific module

[33mcommit c7a0114bb0a36787088c2b07a4c75c4c161769c2[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sun Mar 9 19:25:26 2014 +0100

    moved global engine into game object

[33mcommit 352410d5c174829e8b00b204ccc84d2110852182[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sun Mar 9 19:15:18 2014 +0100

    put players cache in game object rather than in static var

[33mcommit bf5b7a8ec7bc8c4172deae9f5a012dc51551227a[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sun Mar 9 19:08:59 2014 +0100

    updated Portal's docstring

[33mcommit 086a49b13e9f0d83d2bcaa3dd28f9a1adf39f3e0[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sun Mar 9 19:03:45 2014 +0100

    exits no longer share all their properties with their portal.
    instead a list of shared-props can be set on each exit and/or
    on the portal. the default (in static.yml) is to share "closed" and
    "closable".

[33mcommit c57ca461848123cf9fcf5fd354d682d640f44d65[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sun Mar 9 15:01:57 2014 +0100

    use digest instead of plain text password

[33mcommit a8e32c66ed4ce98dca17e9245d82bf31110dc217[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sun Mar 9 14:21:45 2014 +0100

    allow a player to select a description and a gender.

[33mcommit dcf8c38173b6dd2e6511ade4bcbdef31608ca965[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Mar 8 22:27:05 2014 +0100

    make it more 'responsive'

[33mcommit a5c45176f62f480f87e085fd1b39c346133e8552[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Mar 8 21:08:34 2014 +0100

    make input font larger and remove send button

[33mcommit 864cd79ac8d8adda481f6fddfa9289c0c7ef0191[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Mar 8 19:58:54 2014 +0100

    fix typo and don't allow to teleport into an inventory

[33mcommit 19df2af6f92b144b6e11dc873a28daf65c62a4c4[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Mar 8 19:44:34 2014 +0100

    make the department entirely dark.

[33mcommit 8f7c4c6f47f5eda100575ab15ba0566cb19e77a4[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Mar 8 19:40:33 2014 +0100

    added teleport

[33mcommit bbc5a428da8cafd6d7112343faf525270694dfea[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Mar 8 18:56:51 2014 +0100

    you cannot take things from other people's inventories.

[33mcommit 2bde9f8a969e3e53b9c508b25f9aab59beeabb8a[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Mar 8 18:49:46 2014 +0100

    proper names don't take determiners

[33mcommit aaf9b5dfc791b98413667afe54ff7700caab2df2[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Mar 8 18:28:54 2014 +0100

    support game reset button

[33mcommit 6d80eafd70a047d2efbd6134d3abc31181b5baf1[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Mar 8 16:37:59 2014 +0100

    multiuser mode seems to work. you can now 'see' other players.

[33mcommit 9b5f0c3075807529aef04b3ec99459ca3160b95c[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Fri Mar 7 21:15:37 2014 +0100

    addition to README

[33mcommit 26842bfa25549bac6fa6a269fc56610208b2d1e5[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Fri Mar 7 21:10:23 2014 +0100

    added README

[33mcommit 4c8ae619e1915ee7032dfa78df07a9855da80296[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Fri Mar 7 20:46:59 2014 +0100

    game works. added some more actions, events and effects.

[33mcommit 0beb95e7cad4fb761cb246542dc1b873c53a5da0[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Fri Mar 7 18:11:29 2014 +0100

    added data-driven enter-portal events

[33mcommit 8f6f3e0a300edb2bffc80c97c36e35a0c95152b7[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Fri Mar 7 16:49:54 2014 +0100

    save and restore starts to work

[33mcommit e3979b84475da5521ec2e51c24043efbe5180e9b[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Fri Mar 7 12:48:45 2014 +0100

    more debugging, more game working

[33mcommit cd040d7ce6fb7516957f87187b1a9119eabc07d3[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Thu Mar 6 20:40:41 2014 +0100

    ignore .games dir for game saves

[33mcommit 8327f78fff4b0408c5a65ab30102bba74cb21c87[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Thu Mar 6 20:39:16 2014 +0100

    removed mud/models/mixins/described.py

[33mcommit 5a6af4d31abb2885c2fa167b5b2fee8c74c7708c[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Thu Mar 6 20:32:01 2014 +0100

    some more debugging of the game

[33mcommit f952601692a5b01dbcaa8f8fd70d3fa3a9f8627e[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Thu Mar 6 16:45:33 2014 +0100

    debugged the beginning of the game

[33mcommit 5ca57fa7f227a5b6215407990cae0b6a66a3aa39[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 5 21:42:02 2014 +0100

    All global objects have been elminated in favor of a mud.game.GAME
    global object. GAME should never be imported: for testing, it could
    change dynamically.  Instead, you should import mud.game and then
    always use mud.game.GAME to get the current game object.
    
    A Game object does not have to hit the disk: it could be initialized
    with appropriate content instead.

[33mcommit 8ee99fb36e5865915f5dbfa13fef8094f990ee00[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 5 19:03:55 2014 +0100

    added InfoEvent to give a brief description after arriving in a new location

[33mcommit 2829fb114a130f12f0c9c595d67e44c357ae58db[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 5 18:46:21 2014 +0100

    abbrevs: r pour regarder et p pour prendre

[33mcommit b334be95368588aa1fb4c2717325d802bd8310c1[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 5 18:41:16 2014 +0100

    big delta: things start to work

[33mcommit 28740a264e00a3ca4ead685d35abc8e79fb4c6d7[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Mar 5 18:40:14 2014 +0100

    fix the game data

[33mcommit 4e8bcc51fd73cebc14774a66e93cf811b5d711c5[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 19 19:53:00 2014 +0100

    extend events with the ability to have additional effects

[33mcommit 855410d6cee2fc7c0913c1063d8c3eb9e2605cad[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 19 14:27:07 2014 +0100

    add changeprop event

[33mcommit 146b9a4f1335d09163e1f250e100e61c25f967ce[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 19 14:26:29 2014 +0100

    add the notion of effect (a bit like an action but constructed from yaml)

[33mcommit 89d96b765370c1c80c149245d585f476182a51c4[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 19 14:21:03 2014 +0100

    support *toto:foo syntax for props to use an object id toto in the world.
    refactor prop dispatch using _analyze_prop.

[33mcommit c9390fbac832e5681acd50dbc4c3c5a0e65cb9a6[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Feb 18 20:37:34 2014 +0100

    fully support changin properties of the form key:prop where key refers to an object in the context.

[33mcommit 51d8eda630b312ad8d420bc21f370cfcee93821a[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Feb 18 20:30:42 2014 +0100

    give players the can_see computed property

[33mcommit eb1540808cb2943b5b7feb7cbd26f87f632bd1c3[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Feb 18 20:29:57 2014 +0100

    yaml key 'name' can have as a value a string or a list (thus supporting several names)

[33mcommit 24e1e4c99e812ec6b68c2673b5536c069e3ce270[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Feb 18 20:28:31 2014 +0100

    remove the notion of destination from the exit (it's all in the portal)

[33mcommit a80f761086b5df38c384f8f238a5a373529e3ced[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Feb 18 20:27:25 2014 +0100

    add light-on and light-off

[33mcommit 0df13ebf1503d344c29aa9d3ab3061f3d38d059d[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Feb 18 20:17:44 2014 +0100

    simplify event descriptions
    
    for each event type, there is now a key for actor, a key for observer, and
    a key for failed which itself has a subkey for actor and one for observer.

[33mcommit b4a901f51bd313d4f678ba6bbc984a46a7f67a02[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Feb 15 20:18:49 2014 +0100

    handle mud messages in js to create type-appropriate divs

[33mcommit 790873242603944ea66773e0c2a2ca7d5a2a10f0[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Feb 15 20:03:16 2014 +0100

    event api and some events

[33mcommit b4035c8aac966e888303ee9584a601b78086c916[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Feb 15 20:02:28 2014 +0100

    use initial.yml and current.yml in world.py

[33mcommit 374cad5cb869d063dd01ddc75fe24bc7269cc5ea[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Feb 15 20:01:48 2014 +0100

    refactor parser

[33mcommit 3338a0bce4db4eeda3174e05fa322e28412998cf[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Feb 15 20:00:42 2014 +0100

    use static.yml to normalize direction in go action

[33mcommit 18ad0a50e8719d44d848c53f22a46da7b417b2d7[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Feb 15 19:59:34 2014 +0100

    static.yml provides global/default info for the game

[33mcommit 487088ca26785f25ffe57f6159891c7462c8c82f[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Feb 15 19:57:26 2014 +0100

    initial.yml describes all objects in the world's iniial state

[33mcommit de6a08baf88afca4c3324175f299265bdadfa2e3[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Feb 15 18:06:31 2014 +0100

    update models and mixins

[33mcommit aa955cab31fe600ed09c0579f435782a8ec75a34[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Sat Feb 15 18:04:49 2014 +0100

    update actions

[33mcommit 06d71569749ff141be9772e774f71086f0a0c44c[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 5 22:18:48 2014 +0100

    added NotImplemented Basic.short_description_for(player) and Basic.long_description_for(player)

[33mcommit 6aac96f80fe34abbe7be7982ec15b4a35be02597[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 5 22:16:12 2014 +0100

    description of containing things

[33mcommit a30a22ce2cfab3a0da8037c5bfbfac542092eed6[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 5 21:42:56 2014 +0100

    added Player.can_see(loc), Containing.objects_in_vicinity(), Containing.find_prop_in_vicinity(prop)

[33mcommit 3df05a20efa0153c5508366e22d1de779f040ec7[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 5 21:22:56 2014 +0100

    added Propertied.change_prop(p) and Propertied.change_props(ps)

[33mcommit 3f90f1fb8eb81c906e391e6470b6874e05e099b7[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 5 21:12:11 2014 +0100

    added Containing.players() for players in the vicinity

[33mcommit e79401ee74751cfea8ae545e425e7c7cb399cc04[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 5 20:46:04 2014 +0100

    added Exit.other_exit() and Portal.other_exit(e)

[33mcommit 69928695dc20c317409b8a25fee0e30b034fec9f[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 5 20:34:58 2014 +0100

    added support for computing a set of observers

[33mcommit 020dcffb5d82a8fd5e8adbc212aaf85cfa8920e6[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 5 20:34:12 2014 +0100

    added Propertied.is_key_for(obj)

[33mcommit e754233ee6cf482821fc405d88ae1b76d27e9bd0[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 5 20:31:58 2014 +0100

    added support for possibly having several names

[33mcommit 6952b0e25f0944973e336b453821d30261278d24[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Feb 5 14:36:41 2014 +0100

    new version of models using mixins

[33mcommit 298b93512b55f73fb72490ccd1dac72c7ce564d4[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Feb 4 20:46:19 2014 +0100

    start of french parser

[33mcommit e2aeb0184ab311555977b803a0b154e23726a9ae[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Feb 4 20:45:04 2014 +0100

    new simpler and more general properties API

[33mcommit c0ea8abf8eb00028eb1105ed6012bff09fc2ebe0[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Feb 3 09:12:48 2014 +0100

    outline some example actions

[33mcommit 0022893d07a4a7376411f4eeef3166de32164875[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Feb 3 09:11:59 2014 +0100

    load/save the world

[33mcommit 3b8697ade4cf84b7d5c9443e02885330db5c13fb[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Feb 3 09:11:21 2014 +0100

    manage player object in websocket

[33mcommit bbb04ab6c012bd47c4d4dd45a76c2ca8e8302444[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Feb 3 09:10:25 2014 +0100

    models and model-mixins

[33mcommit 4ac0115257a92b5e21c62ac01e75922c85096dd2[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Jan 22 20:37:16 2014 +0100

    moved player to models

[33mcommit e00cef1bafd910488459e0fea6168bacbfdc5fa3[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Jan 20 21:28:27 2014 +0100

    basic chat works again

[33mcommit 29ab70efbb600a44baec97fc1f2ebd7486f11429[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Jan 20 21:13:28 2014 +0100

    more player, start of engine

[33mcommit e0bcc300b0361cbc64403c29cf9bd0c4b6eeb1ba[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Mon Jan 20 21:12:27 2014 +0100

    added player and more

[33mcommit 8b0ed7ebbbc3d648b9e675c10398cc398ffb5aa4[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Wed Jan 15 11:58:48 2014 +0100

    basic chat + transcripts works

[33mcommit c031a92c3a8947e8367c10b8144d9df560282036[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Jan 14 21:07:52 2014 +0100

    basic communication through websockets

[33mcommit b3e47a649d39a24f66476b2d8a21643cf7222d59[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Jan 14 16:57:24 2014 +0100

    non js play/post

[33mcommit 0b78d374ed8a2e05ecf5427456030ad2cb78e1af[m
Author: Denys Duchier <denys.duchier@univ-orleans.fr>
Date:   Tue Jan 14 15:55:10 2014 +0100

    start of mud infrastructure
